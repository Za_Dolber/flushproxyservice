﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Runtime.InteropServices;
using Microsoft.Win32;

namespace FlushProxySettings_service
{
    public partial class DeleteProxyService : ServiceBase
    {
        Timer timer = new Timer(); // name space(using System.Timers;)  
        public DeleteProxyService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args) //setup timer
        {
            WriteToFile("Service is started at " + DateTime.Now);
            timer.Elapsed += new ElapsedEventHandler(OnElapsedTime);
            timer.Interval = 1000*30; //every thirty seconds
            timer.Enabled = true;
        }

        [DllImport("wininet.dll")]
        private static extern bool
        InternetSetOption(IntPtr hInternet, int dwOption, IntPtr lpBuffer, int dwBufferLength);

        private const int INTERNET_OPTION_SETTINGS_CHANGED = 39;
        private const int INTERNET_OPTION_REFRESH = 37;

        public static void Reset()
        {
            var regKey = Registry.CurrentUser.OpenSubKey
                ("Software\\Microsoft\\Windows\\CurrentVersion\\Internet Settings", true);
            regKey.SetValue("ProxyEnable", 0);
            regKey.SetValue("ProxyServer", 0);

            if ((int)regKey.GetValue("ProxyEnable", 1) == 1)
                WriteToFile("Unable to disable the proxy.");
            else
                WriteToFile("The proxy has been turned off.");       

            var settingsReturn = InternetSetOption(IntPtr.Zero, INTERNET_OPTION_SETTINGS_CHANGED, IntPtr.Zero, 0);
            var refreshReturn = InternetSetOption(IntPtr.Zero, INTERNET_OPTION_REFRESH, IntPtr.Zero, 0);
            WriteToFile("Reset called " + DateTime.Now);
        }


        protected override void OnStop()
        {
            WriteToFile("Service is stopped at " + DateTime.Now);
        }
        private void OnElapsedTime(object source, ElapsedEventArgs e)
        {
            WriteToFile("Proxy flushed " + DateTime.Now);
            Reset();
        }
        public static void WriteToFile(string Message)
        {
            string path = AppDomain.CurrentDomain.BaseDirectory + "\\Logs";
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            string filepath = AppDomain.CurrentDomain.BaseDirectory 
                                        + "\\Logs\\ServiceLog_" 
                                        + DateTime.Now.Date.ToShortDateString().Replace('/', '_') + ".txt";
            if (!File.Exists(filepath))
            {
                // Create a file to write to.   
                using (StreamWriter sw = File.CreateText(filepath))
                {
                    sw.WriteLine(Message);
                }
            }
            else
            {
                using (StreamWriter sw = File.AppendText(filepath))
                {
                    sw.WriteLine(Message);
                }
            }
        }
    }
}
